#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"

bool input(char *str);
void get_str(int n, int m, char str[n][m]);
void print_string(int n, int m, char str[n][m]);
void print_sort (int n, int m, int i, char str[n][m]) ;
void sort_string (int n, int m, char str[n][m]) ;
void bubble_sort (int n, int m, int i,char str[n][m]) ;

int main(void)
{
    int n = 5;
    int m = 9;
    puts("Choose:\n(1) To select strings to sort\n(2) To run the example");
    int choice = 0;
    scanf("%i", &choice);
    if (choice == 1)
    {
        printf(KGRN"Input the number of strings:\n" RESET) ;
        scanf("%i", &n);
    }
    else if (choice == 2)
    {
        n = 6;
    }
    else 
    {
        printf (KCYN"ERROR.Wrong input\n"RESET) ;
        exit(1) ;
    }
    char str[n][m];
    if (choice == 1)
    {
        get_str(n, m, str);
    }
    else if(choice == 2)
    {
        strcpy(str[0], "aa12345b\0") ;
        strcpy(str[1], "ba23456c\0") ;
        strcpy(str[2], "aa22346b\0") ;
        strcpy(str[3], "aa12346c\0") ;
        strcpy(str[4], "aa12345c\0") ;
        strcpy(str[5], "bb11111a\0") ;
    }
    printf(KGRN"\nThe string array to sort:\n" RESET) ;
    print_string(n, m, str);
    sort_string (n, m, str) ;
    printf(KGRN"Sorted array\n" RESET) ;
    print_string(n, m, str) ;
    
}

void get_str(int n, int m, char str[n][m])
{
    char arr[10];
    puts("Input strings in format XX00000X:");
    getchar() ;
    for (int i = 0; i < n; i++)
    {
        printf("string %i:\n", i);
        fgets(arr, 10, stdin);
        if (arr[strlen(arr) - 1] == '\n')
        {
            arr[strlen(arr) - 1] = 0;
        }
        if (input(arr))
        {
            strncpy(str[i], arr, 9);
        }
        else
        {
            printf (KCYN"ERROR.Wrong input\n"RESET) ;
            exit(1);
        }
    }
}

void print_string(int n, int m, char str[n][m])
{
    for (int i = 0; i < n; i++)
    {
        puts(str[i]);
    }
}

void print_sort (int n, int m, int i, char str[n][m])
{
    for(int j = 0; j<n; j++)
    {
        for(int k =0; k<m; k++)
        {
            if(k == i)
            {
                printf(KRED "%c" RESET, str[j][k]) ;
            }
            else
            {
                printf("%c", str[j][k]) ;
            }
            
        }
        putchar('\n') ;
    }
}

bool input(char *str)
{
    if (strlen(str) != 8)
        return false;

    if (!isalpha(str[0]) || !isalpha(str[1]) || !isalpha(str[7]))
        return false;

    for (int i = 2; i < 7; i++)
    {
        if (!isdigit(str[i]))
            return false;
    }

    return true;
}

void sort_string (int n, int m, char str[n][m]) 
{
    for(int i = m-1; i>=0; i--)
    {
        bubble_sort(n, m, i, str) ;
        printf(KMAG "Sort %i\n" RESET, m-i) ;
        print_sort(n, m, i, str) ;
    }
}

void bubble_sort (int n, int m, int i,char str[n][m]) 
{
    for (int j = 0; j<n-1; j++)
    {
        for(int k =0; k<n-j-1; k++)
        {
            if(str[k][i]<str[k+1][i])
            {
                char buffer[10] ;
                strcpy(buffer, str[k]) ;
                strcpy(str[k], str[k+1]) ;
                strcpy(str[k+1], buffer) ;
                strcpy(buffer, "\0") ;
            }
        }
    }
}
 