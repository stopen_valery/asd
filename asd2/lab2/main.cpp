#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cstring>

#define KGRN "\x1B[32m"
#define KMAG "\x1B[35m"
#define RESET "\x1B[0m"

struct Stack_Node
{
    char data;
    struct Stack_Node *next;
};
struct Stack_Node *create_node(char data)
{
    struct Stack_Node *new_node = new (struct Stack_Node);
    new_node->data = data;
    new_node->next = NULL;
}
struct Stack_Node *add_node(struct Stack_Node *head, char data)
{
    struct Stack_Node *save_head = head;
    struct Stack_Node *new_node = create_node(data);
    if (head == NULL)
    {
        return new_node;
    }
    while (head->next != NULL)
    {
        head = head->next;
    }
    head->next = new_node;
    return save_head;
}
char top(struct Stack_Node *head)
{
    struct Stack_Node *save_head = head;
    while (save_head->next != NULL)
    {
        save_head = save_head->next;
    }
    return save_head->data;
}

struct Stack_Node *del_last(struct Stack_Node *head)
{
    if (head->next == NULL)
    {
        struct Stack_Node *del = head;
        head = NULL;
        delete del;
        return head;
    }
    struct Stack_Node *save_head = head;
    while (head->next->next != NULL)
    {
        head = head->next;
    }
    struct Stack_Node *del = head->next;
    head->next = NULL;
    delete del;
    return save_head;
}
void print_node(struct Stack_Node *head)
{
    while (head != NULL)
    {
        std::cout << head->data;
        head = head->next;
    }
    std::cout << "\n";
}

struct Stack_Node *to_revpol(std::string &in);

int main()
{
    std::string input;
    std::cout << "Enter:\n1.To run an example\n2.To enter an equation\n";
    int n = 0;
    std::cin >> n;
    if (n == 2)
    {
        std::cin >> input;
    }
    else if (n == 1)
    {
        input = "(4+5)*x-(y/2+15)";
        std::cout << "Example: ";
        std::cout << input << "\n";
    }
    else
    {
        exit(1);
    }
    struct Stack_Node *res = to_revpol(input);
    print_node(res);
}
struct Stack_Node *to_revpol(std::string &input)
{
    struct Stack_Node *temp = NULL;
    struct Stack_Node *result = NULL;
    for (auto i : input)
    {
        if (i == '.')
        {
            result = add_node(result, i);
            std::cout << KGRN << "Result Stack: " << RESET;
            print_node(result);
        }
        else if ((i >= '0' && i <= '9') ||
                 (i >= 'a' && i <= 'z') ||
                 (i >= 'A' && i <= 'Z'))
        {
            if (result != NULL &&
                (top(result) >= '0' && top(result) <= '9') &&
                !(i >= '0' && i <= '9'))
            {
                temp = add_node(temp, '*');
                std::cout << KMAG << "Temp Stack: " << RESET;
                print_node(temp);
                result = add_node(result, ';');
                std::cout << KGRN << "Result Stack: " << RESET;
                print_node(result);
            }
            result = add_node(result, i);
            std::cout << KGRN << "Result Stack: " << RESET;
            print_node(result);
        }
        else if ((i == '+') || (i == '-') || (i == '*') || (i == '/'))
        {
            if (temp != NULL && (top(temp) == '*' || top(temp) == '/'))
            {
                if (i == '*' || i == '/')
                {
                    result = add_node(result, top(temp));
                    temp = del_last(temp);
                    temp = add_node(temp, i);
                    std::cout << KMAG << "Temp Stack: " << RESET;
                    print_node(temp);
                    std::cout << KGRN << "Result Stack: " << RESET;
                    print_node(result);
                }
                else if (i == '+' || i == '-')
                {
                    while (temp != NULL && top(temp) != '(')
                    {
                        result = add_node(result, top(temp));
                        temp = del_last(temp);
                        std::cout << KMAG << "Temp Stack: " << RESET;
                        print_node(temp);
                        std::cout << KGRN << "Result Stack: " << RESET;
                        print_node(result);
                    }
                    temp = add_node(temp, i);
                    std::cout << KMAG << "Temp Stack: " << RESET;
                    print_node(temp);
                }
            }
            else
            {
                if (result != NULL && ((top(result) >= '0' && top(result) <= '9') ||
                                       (top(result) >= 'a' && top(result) <= 'z') ||
                                       (top(result) >= 'A' && top(result) <= 'Z')))
                {
                    result = add_node(result, ';');
                    std::cout << KGRN << "Result Stack: " << RESET;
                    print_node(result);
                }
                else if (((temp != NULL && top(temp) == '(') || result == NULL) && i == '-')
                {
                    result = add_node(result, '_');
                    std::cout << KGRN << "Result Stack: " << RESET;
                    print_node(result);
                }
                if (top(result) != '_')
                {
                    temp = add_node(temp, i);
                    std::cout << KMAG << "Temp Stack: " << RESET;
                    print_node(temp);
                }
            }
        }
        else if (i == '(')
        {
            if (result != NULL && ((top(result) >= '0' && top(result) <= '9') ||
                                   (top(result) >= 'a' && top(result) <= 'z') ||
                                   (top(result) >= 'A' && top(result) <= 'Z')))
            {
                result = add_node(result, ';');
                std::cout << KGRN << "Result Stack: " << RESET;
                print_node(result);
            }
            temp = add_node(temp, i);
            std::cout << KMAG << "Temp Stack: " << RESET;
            print_node(temp);
        }
        else if (i == ')')
        {
            while (temp != NULL && top(temp) != '(')
            {
                result = add_node(result, top(temp));
                std::cout << KGRN << "Result Stack: " << RESET;
                print_node(result);
                temp = del_last(temp);
                std::cout << KMAG << "Temp Stack: " << RESET;
                print_node(temp);
            }
            if (temp != NULL)
            {
                temp = del_last(temp);
                std::cout << KMAG << "Temp Stack: " << RESET;
                print_node(temp);
            }
            else
            {
                throw "WRONG INPUT! Missing (";
            }
        }
    }
    while (temp != NULL)
    {
        result = add_node(result, top(temp));
        std::cout << KGRN << "Result Stack: " << RESET;
        print_node(result);
        temp = del_last(temp);
        std::cout << KMAG << "Temp Stack: " << RESET;
        print_node(temp);
    }
    return result;
}