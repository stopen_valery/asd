#include "hashtable.h"
#include <iostream>
#include <algorithm>

#define KGRN "\x1B[32m"
#define KMAG "\x1B[35m"
#define RESET "\x1B[0m"

HashTable::HashTable()
{
    size = 3;
    capacity = 0;
}

HashTable::~HashTable() {}

void HashTable::insert(key in_key, value in_value)
{
    int code = hash_code(in_key);
    int index = hash_index(code);
    key_value in = {in_key, in_value};
    table.at(index).val.push_back(in);
    capacity++;
    int loadness = capacity/size ;
    if(loadness==1)
    {
        this->rehashing() ;
        cout <<KMAG<< "rehashing\n"<<RESET ;
    }
}

void HashTable::rehashing()
{
    this->size = size * 2;
    vector<table_val> new_table{size};
    for (int i = 0; i < size / 2; i++)
    {
        if (!table.at(i).val.empty())
        {
            for (key_value v : table.at(i).val)
            {
                int code = hash_code(v.key_);
                int index = hash_index(code);
                key_value in {v.key_, v.value_} ;
                new_table.at(index).val.push_back(in) ;
            }
        }
    }
    this->table = new_table;
}

int HashTable::hash_code(key code_key)
{
    int code = 0;
    for (int i = 0; code_key.first_name[i] != '\0'; i++)
    {
        int temp = (int)code_key.first_name[i];
        code += temp;
    }
    for (int i = 0; code_key.last_name[i] != '\0'; i++)
    {
        int temp = (int)code_key.last_name[i];
        code += temp;
    }
    return code;
}

int HashTable::hash_index(int code)
{
    return code % size;
}

bool HashTable::remove(key rem_key)
{
    int code = hash_code(rem_key);
    int index = hash_index(code);
    if (table.at(index).val.empty())
    {
        return false;
    }
    if (table.at(index).val.front().key_.first_name == rem_key.first_name && table.at(index).val.front().key_.last_name == rem_key.last_name)
    {
        table.at(index).val.pop_front();
        capacity-- ;
        cout << KMAG << "Deleted successfully\n"
             << RESET;
        return true;
    }
    else if (table.at(index).val.back().key_.first_name == rem_key.first_name && table.at(index).val.back().key_.last_name == rem_key.last_name)
    {
        table.at(index).val.pop_back();
        capacity-- ;
        cout << KMAG << "Deleted successfully\n"
             << RESET;
        return true;
    }
}

key_value HashTable::find_at(key find_key)
{
    int code = hash_code(find_key);
    int index = hash_index(code);
    if (table.at(index).val.front().key_.first_name == find_key.first_name && table.at(index).val.front().key_.last_name == find_key.last_name)
    {
        return table.at(index).val.front();
    }
    for (key_value v : table.at(index).val)
    {
        if (v.key_.first_name == find_key.first_name && v.key_.last_name == find_key.last_name)
        {
            return v;
        }
    }
    key_value v{"", "", 0, ""};
    cout << KMAG << "There in no patient with such key\n" << RESET;
    return v;
}

void HashTable::print()
{
    for (int i = 0; i < size; i++)
    {
        int c = 1 ;
        cout<< "\nSlot " <<i <<"\n";
        if (!table.at(i).val.empty())
        {
            for (key_value v : table.at(i).val)
            {
                std::cout << c << ". ";
                std::cout << KGRN << "First name: " << RESET << v.key_.first_name << " ";
                std::cout << KGRN << "Last name: " << RESET << v.key_.last_name << "\n";
                std::cout << KGRN << "Doctor: " << RESET << v.value_.doc_name << " ";
                std::cout << KGRN << "ID : " << RESET << v.value_.patient_ID << "\n";
                c++ ;
            }
        }
        else
        {
            cout<<KMAG<<"empty\n"<<RESET ;
        }
        
    }
}