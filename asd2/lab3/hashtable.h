#pragma once
#include <vector>
#include <list>
#include <string>

using namespace std;

struct key
{
    string first_name;
    string last_name;
};

struct value
{
    int patient_ID;
    string doc_name;
};

struct key_value
{
    key key_;
    value value_;
};

struct table_val
{
    list<key_value> val;
};

class HashTable
{
    vector<table_val> table{3};
    size_t size;
    size_t capacity;

public:
    HashTable();
    ~HashTable();
    void rehashing();
    void insert(key in_key, value in_value);
    bool remove(key rem_key);
    key_value find_at(key find_key);
    int hash_code(key code_key);
    int hash_index(int code);
    void print();
};
