#include "hashtable2.h"
#include <iostream>
#include <algorithm>

#define KGRN "\x1B[32m"
#define KMAG "\x1B[35m"
#define RESET "\x1B[0m"

HashTable2::HashTable2()
{
    size = 3;
    capacity = 0;
}

HashTable2::~HashTable2() {}

void HashTable2::insert(doc_key in_key, doc_value in_value)
{
    int code = hash_code(in_key);
    int index = hash_index(code);
    doc_key_value in = {in_key, in_value};
    if (table.at(index).val.empty())
    {
        capacity++;
    }
    table.at(index).val.push_back(in);

    int loadness = capacity / size;
    if (loadness >= 1)
    {
        this->rehashing();
    }
}

void HashTable2::rehashing()
{
    this->size = size * 2;
    vector<doc_table_val> new_table{size};
    for (int i = 0; i < size / 2; i++)
    {
        if (!table.at(i).val.empty())
        {
            for (doc_key_value v : table.at(i).val)
            {
                int code = hash_code(v.key_);
                int index = hash_index(code);
                doc_key_value in{v.key_, v.value_};
                new_table.at(index).val.push_back(in);
            }
        }
    }
    this->table = new_table;
}

int HashTable2::hash_code(doc_key code_key)
{
    int code = 0;
    for (int i = 0; code_key.name[i] != '\0'; i++)
    {
        int temp = (int)code_key.name[i];
        code += temp;
    }
    return code;
}

int HashTable2::hash_index(int code)
{
    return code % size;
}

bool HashTable2::remove(doc_key rem_key, doc_value rem_name)
{
    int code = hash_code(rem_key);
    int index = hash_index(code);
    if (table.at(index).val.front().key_.name == rem_key.name && rem_name.patient_name == table.at(index).val.front().value_.patient_name && rem_name.patient_surname == table.at(index).val.front().value_.patient_surname)
    {
        table.at(index).val.pop_front();
        capacity--;
        return true;
    }
    else if (table.at(index).val.back().key_.name == rem_key.name && rem_name.patient_name == table.at(index).val.back().value_.patient_name && rem_name.patient_surname == table.at(index).val.back().value_.patient_surname)
    {
        table.at(index).val.pop_back();
        capacity--;
        return true;
    }
    return false;
}

doc_table_val HashTable2::find_at(doc_key find_key)
{
    int code = hash_code(find_key);
    int index = hash_index(code);
    doc_table_val ret{};
    for (doc_key_value v : table.at(index).val)
    {
        if (v.key_.name == find_key.name)
        {
            ret.val.push_back(v);
        }
    }
    return ret;
    cout << KMAG << "There in no user with such key" << RESET;
}

void HashTable2::print()
{
    for (int i = 0; i < size; i++)
    {
        if (!table.at(i).val.empty())
        {
            for (doc_key_value v : table.at(i).val)
            {
                std::cout << i << ". ";
                std::cout << KGRN << "Doctor: " << RESET << v.key_.name << " ";
                std::cout << KGRN << "First name: " << RESET << v.value_.patient_name << " ";
                std::cout << KGRN << "Last name: " << RESET << v.value_.patient_surname << "\n";

                std::cout << KGRN << "ID : " << RESET << v.value_.patient_ID << "\n";
            }
        }
    }
}