#pragma once
#include <vector>
#include <list>
#include <string>

using namespace std;

struct doc_key
{
    string name;
};

struct doc_value
{
    int patient_ID;
    string patient_name;
    string patient_surname;
};

struct doc_key_value
{
    doc_key key_;
    doc_value value_;
};

struct doc_table_val
{
    list<doc_key_value> val;
};

class HashTable2
{
    vector<doc_table_val> table{3};
    size_t size;
    size_t capacity;

public:
    HashTable2();
    ~HashTable2();
    void insert(doc_key in_key, doc_value in_value);
    bool remove(doc_key rem_key, doc_value rem_name);
    doc_table_val find_at(doc_key find_key);
    int hash_code(doc_key code_key);
    int hash_index(int code);
    void rehashing();
    void print();
};