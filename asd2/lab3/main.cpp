#include <iostream>
#include "hashtable.h"
#include "hashtable2.h"
#define KGRN "\x1B[32m"
#define KMAG "\x1B[35m"
#define RESET "\x1B[0m"

int main()
{
    HashTable test{};
    HashTable2 test_{};
    while (1)
    {
        std::cout << KMAG << "Menu:\n"
                  << RESET << "1.Add patient\n"
                  << "2.Delete patient\n"
                  << "3.Find patient\n"
                  << "4.Show storage\n"
                  << "5.Show doctor by name\n"
                  << "6.Exit\n";
        int n = 0;
        cin >> n;
        if (n == 1)
        {
            string f_name;
            string l_name;
            int ID;
            string doc;
            std::cout << "Input first name: ";
            std::cin >> f_name;
            std::cout << "Input last name: ";
            std::cin >> l_name;
            std::cout << "Input ID: ";
            std::cin >> ID;
            std::cout << "Input doctor`s name: ";
            std::cin >> doc;
            key in_k{f_name, l_name};
            value in_v{ID, doc};
            test.insert(in_k, in_v);
            doc_key in_k2 = {doc};
            doc_value in_v2 = {ID, f_name, l_name};
            test_.insert(in_k2, in_v2);
        }
        else if (n == 2)
        {
            string f_name;
            string l_name;
            cout << "Input first name: ";
            cin >> f_name;
            cout << "Input last name: ";
            cin >> l_name;
            key in_k{f_name, l_name};
            key_value val = test.find_at(in_k);
            doc_key r_k{val.value_.doc_name};

            if (test.remove(in_k))
            {
                doc_value r_v{0, f_name, l_name};
                test_.remove(r_k, r_v);
                test.print() ;
            }
        }
        else if (n == 3)
        {
            string f_name;
            string l_name;
            cout << "Input first name: ";
            cin >> f_name;
            cout << "Input last name: ";
            cin >> l_name;
            key in_k{f_name, l_name};
            key_value val = test.find_at(in_k);
            if (!val.key_.first_name.empty())
            {
                std::cout << KGRN << "First name: " << RESET << val.key_.first_name << " ";
                std::cout << KGRN << "Last name: " << RESET << val.key_.last_name << "\n";
                std::cout << KGRN << "Doctor: " << RESET << val.value_.doc_name << " ";
                std::cout << KGRN << "ID: " << RESET << val.value_.patient_ID << "\n";
            }
        }
        else if (n == 4)
        {
            cout << KMAG << "The table of patients:\n"
                 << RESET;
            test.print();
        }
        else if (n == 5)
        {
            string d_name;
            std::cout << "Input the name of a doctor: ";
            cin >> d_name;
            doc_key name = {d_name};
            doc_table_val ret = test_.find_at(name);
            for (doc_key_value v : ret.val)
            {
                std::cout << KGRN << "Doctor:" << RESET << v.key_.name << "\n";
                std::cout << KGRN << "First name: " << RESET << v.value_.patient_name << " ";
                std::cout << KGRN << "Last name: " << RESET << v.value_.patient_surname << " ";
                std::cout << KGRN << "ID: " << RESET << v.value_.patient_ID << "\n";
            }
        }
        else if (n == 6)
        {
            exit(0);
        }
        else
        {
            cout << "There is no such option\n";
        }
    }
}