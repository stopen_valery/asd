#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"

#include "avl_tree.h"

using namespace std ;

int avl_tree::height(avl *tree)
{
    int h = 0;
    if (tree != NULL)
    {
        int l_height = height(tree->left);
        int r_height = height(tree->right);
        int max_height = max(l_height, r_height);
        h = max_height + 1;
    }
    return h;
}
int avl_tree::difference(avl *tree)
{
    int l_height = height(tree->left);
    int r_height = height(tree->right);
    int dif = l_height - r_height;
    return dif;
}
avl *avl_tree::rr_rotate(avl *parent)
{
    avl *tree;
    tree = parent->right;
    parent->right = tree->left;
    tree->left = parent;
    cout<<KGRN<<"Right-Right Rotation\n"<<RESET;
    return tree;
}
avl *avl_tree::ll_rotate(avl *parent)//left left rotation
{
    avl *tree;
    tree = parent->left;
    parent->left = tree->right;
    tree->right = parent;
    cout<<KGRN<<"Left-Left Rotation\n"<<RESET;
    return tree;
}
avl *avl_tree::lr_rotate(avl *parent)//left right rotation
{
    avl *tree;
    tree = parent->left;
    parent->left = rr_rotate(tree);
    cout<<KGRN<<"Left-Right Rotation\n"<<RESET;
    return ll_rotate(parent);
}
avl *avl_tree::rl_rotate(avl *parent)//right left rotation
{
    avl *tree;
    tree = parent->right;
    parent->right = ll_rotate(tree);
    cout<<KGRN<<"Right-Left Rotation\n"<<RESET;
    return rr_rotate(parent);
}
avl *avl_tree::balance(avl *tree)
{
    int bal_factor = difference(tree);
    if (bal_factor > 1)
    {
        if (difference(tree->left) > 0)
            tree = ll_rotate(tree);
        else
            tree = lr_rotate(tree);
    }
    else if (bal_factor < -1)
    {
        if (difference(tree->right) > 0)
            tree = rl_rotate(tree);
        else
            tree = rr_rotate(tree);
    }
    return tree;
}

avl *avl_tree::insert(avl *root, int value)
{
    if (root == NULL)
    {
        root = new avl;
        root->data = value;
        root->left = NULL;
        root->right= NULL;
        return root;
    }
    else if (value< root->data)
    {
        root->left= insert(root->left, value);
        root = balance(root);
    }
    else if (value > root->data)
    {
        root->right= insert(root->right, value);
        root = balance(root);
    }
    else if (value == root->data)
    {
        cout<<KRED<<"ERROR. The key should be unique\n"<<RESET;
        return root ;
    }
    return root;
}

avl *avl_tree::delete_right(avl *root)
{
    avl* save = root ;
    if(root == NULL)
    {
        cout<<KRED<<"ERROR. The tree is empty, nothing to delete\n"<<RESET;
        exit (0) ;
    }
    else if(root->right==NULL)
    {
        cout<<KRED<<"ERROR. The tree has no right leaf\n"<<RESET;
        exit(0) ;
    }
    while (root->right->right !=NULL)
    {
        root = root->right ;
    }
    if(root->right->left == NULL)
    {
        avl *del = root->right ;
        root->right = NULL ;
        delete (del) ;
    }
    else if(root->right->left!=NULL)
    {
        avl *del = root->right ;
        avl *del_2 = root->right->left ;
        int in = del_2->data ;
        root->right->left = NULL ;
        delete (del_2) ;
        root->right = NULL ;
        delete(del) ;
        save = insert(save, in) ;
    }
    return save ;

}

static void printValueOnLevel(avl *node, char pos, int depth)
{
    for (int i = 0; i < depth; i++)
    {
        std::cout << "....";
    }
    std::cout << pos <<" ";

    if (node == nullptr)
    {
        std::cout << "(null)\n";
    }
    else
    {
        std::cout << node->data <<"\n";
    }
}
static void printNode(avl *node, char pos, int depth)
{
    bool hasChild = node != nullptr && (node->left != nullptr || node->right != nullptr);
    printValueOnLevel(node, pos, depth);
    if (hasChild)
    {
        printNode(node->left, 'L', depth + 1);
    }
    if (hasChild)
    {
        printNode(node->right, 'R', depth + 1);
    }
}
void avl_tree::show()
{
    printNode(tree, '+', 0);
}
