#ifndef AVL_TREE_H
#define AVL_TREE_H
#pragma once
#include<iostream>
#include<cstdio>
#include<sstream>
#include<algorithm>
#include "avl.h"
#define pow2(n) (1 << (n))

using namespace std ;

class avl_tree
{
   public:
   avl *tree ;

   int height(avl *);
   int difference(avl *);

   avl * rr_rotate(avl *);
   avl * ll_rotate(avl *);
   avl * lr_rotate(avl*);
   avl * rl_rotate(avl *);

   avl * balance(avl *);
   avl * insert(avl *, int) ;

   avl *delete_right(avl *) ;

   void show();
   avl_tree()
   {
        tree = NULL ;
   }
};
#endif // AVL_TREE_H
