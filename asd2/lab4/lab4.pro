QT += core
QT -= gui

TARGET = lab4
CONFIG += console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += -std=c++17

TEMPLATE = app

SOURCES += main.cpp \
    avl_tree.cpp

HEADERS += \
    avl_tree.h \
    avl.h

