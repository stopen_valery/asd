#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"
#include <QCoreApplication>

#include "avl_tree.h"

using namespace std ;

int main()
{
    int c, i;
    avl_tree avl;
    while (1)
    {
        cout << "1.Insert" << endl;
        cout << "2.Exit" << endl;
        cin >> c;
        switch (c) //perform switch operation
        {
        case 1:
            cout << KCYN<<"Enter value to be inserted: "<<RESET;
            cin >> i;
            avl.tree = avl.insert(avl.tree, i);
            if (avl.tree == NULL)
            {
                cout <<KRED<< "Tree is Empty" <<RESET<< endl;
                continue;
            }
            cout <<KGRN << "Balanced AVL Tree:" << endl<<RESET;
            avl.show();
            cout<<endl;
            break;
        case 2:
            avl.tree = avl.delete_right(avl.tree) ;
            cout <<KGRN << "Deleting the last right leaf:" << endl<<RESET;
             avl.show();
            exit(0);
            break;
        default:
            cout <<KRED<< "Wrong Choice" <<RESET<< endl;
        }
    }
    return 0;
}
