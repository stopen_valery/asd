#include <stdio.h>
#include <stdbool.h>
int CorrectData (int d1, int d2, int m1, int m2)
{
    bool mon = ((m1<=12)&&(m2<=12)) ;
    bool day = ((d2<=31)&&(d1<=31)) ;
    int cd = 0;
    if ((mon==1)&&(day==1))
    {
        cd = 1 ;
    }
    else
    {
        cd = 0 ;
    }
    return cd ;
}
int CorrectInput(int d1,int d2,int m1,int m2,int y1,int y2)
{
    int ci = 0 ;
    bool year = y1<y2 ;
    bool syear = y2==y1 ;
    bool day = d1<d2 ;
    bool month = m1<m2 ;
    bool smonth = m1==m2 ;
    if ((year == 1)||((syear==1)&&(month == 1)))
    {
        ci = 1 ;
    }
    else 
    {
        if ((smonth==1)&&(day==1))
        {
            ci = 1 ;
        }
        else
        {
            ci = 0 ;
        }
    }
    return ci ;
}
int LeapYear (int y)
{
    bool ly1 = ((y%4==0)&&(y%100!=0)) ;
    bool ly2 = ((y%100==0)&&(y%400==0)) ;
    int ly = 0 ;
    if ((ly1==1)||(ly2==1))
    {
        ly = 366 ;
    }
    else 
    {
        ly = 365 ;
    }
    return ly ;
}
int Month (int mon, int ly)
{
    int m = 0 ;
    if (mon==2)
    {
        if (ly == 366)
        {
            m=29 ;
        }
        else 
        {
            m=28;
        }
    }
    else if ((mon<=7 && mon%2==0) || (mon>7 && mon%2==1))
    {
        m=30 ; 
    }
    else 
    {
        m = 31 ;
    }
    return m ;
}
int main()
{
    unsigned int d1 = 0;
    unsigned int d2 = 0;
    unsigned int m1 = 0;
    unsigned int m2 = 0;
    unsigned int y1 = 0;
    unsigned int y2 = 0;
    int d = 0;
    int y = 0;
    int ly = 0;
    printf ("Input the initial day d/m/y: ") ;
    scanf ("%i %i %i", &d1, &m1, &y1) ;
    printf ("Input the final day d/m/y: ") ;
    scanf ("%i %i %i", &d2, &m2, &y2) ;
    int cd = CorrectData (d1, d2, m1, m2) ;
    int ci = CorrectInput(d1, d2, m1, m2, y1, y2) ;
    if (cd == 0)
    {
        printf ("Wrong data\n") ;
    }
    else if (ci == 0)
    {
        printf ("Wrong order\n") ;
    } 
    else
    { 
        if ((m1<m2)||(m1==m2 && d1<d2))
        {
            y = y2 - y1 ;
            for (int i = y1; i<y2; i++)
            {
                ly = LeapYear(i) ;
                d = d + ly ;
            }
            for (int j = m1; j<m2; j++)
            {
                int m = Month(j , ly) ;
                d = d + m ;
            }
        }
        else
        {
            y = (y2 - y1)-1 ;
            for (int i = y1; i<(y2-1); i++)
            {
                ly = LeapYear(i) ;
                d = d + ly ;
            }
            for (int j = m1; j<=12; j++)
            {
                int m = Month(j , ly) ;
                d = d + m ;
            }
            for (int j = 1; j<m2; j++)
            {
                int m = Month(j , ly) ;
                d = d + m ;
            }
        
        }
        d = d + (d2-d1) ;
        printf("Years: %i\n" , y) ;
        printf ("Days: %i\n", d) ;
    }

}