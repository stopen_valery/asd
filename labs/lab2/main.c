#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main()
{
    srand(time(0));
    int n = 0;
    int m = 0;
    printf("Input the size of matrix (n, m):\n");
    scanf("%i %i", &n, &m);
    if ((n != m) || (n <= 0) || (m <= 0))
    {
        printf("ERROR. The wrong size input\n");
    }
    else
    {
        int array[n][m];
        int left = (n - 1) % 3;
        int num = (n - 1) / 3;
        int k = n - 2;
        int d = n - 2;
        int col = 0;
        int command = 0;
        printf("Input:\n1 to make random matrix\n2 to make cheking matrix\n");
        scanf("%i", &command);
        if (command == 1)
        {
            printf("Random matrix %ix%i:\n", n, m);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    array[i][j] = rand() % (99 - 10 + 1) + 10;
                    printf("%i ", array[i][j]);
                }
                puts("\n");
            }
        }
        else if (command == 2)
        {
            printf("Checking matrix %ix%i:\n", n, m);
            int count = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    array[i][j] = count;
                    printf("%3i ", array[i][j]);
                    count++;
                }
                puts("\n");
            }
        }
        else
        {
            printf("ERROR. Wrong command input\n");
            return 0;
        }
        int min = array[n - k][0];
        int x_min = n - k ;
        int y_min = 0 ;
        printf("Over the diagonal:\n");
        for (col = 0; col < num; col++)
        {
            for (int ver = k; ver > col; ver--)
            {
                printf("%i[%i][%i] ", array[ver][col], ver, col);
                if (array[ver][col] < min)
                {
                    min = array[ver][col];
                    x_min = ver ;
                    y_min = col ;
                }
            }
            for (int hor = col; hor < k; hor++)
            {
                printf("%i[%i][%i] ", array[col][hor], col, hor);
                if (array[col][hor] < min)
                {
                    min = array[col][hor];
                    x_min = col ;
                    y_min = hor ;
                }
            }
            for (int dia1 = col; dia1 < k; dia1++)
            {
                for (int dia2 = k; dia2 > col; dia2--)
                {
                    if (dia1 + dia2 == d)
                    {
                        printf("%i[%i][%i] ", array[dia1][dia2], dia1, dia2);
                        if (array[dia1][dia2] < min)
                        {
                            min = array[dia1][dia2];
                            x_min = dia1 ;
                            y_min = dia2 ;
                        }
                    }
                }
            }
            k = k - 2;
            d--;
        }
        if (left == 1)
        {
            for (int ver = k; ver > (col - 1); ver--)
            {
                printf("%i[%i][%i] ", array[ver][col], ver, col);
                if (array[ver][col] < min)
                {
                    min = array[ver][col];
                    x_min = ver ;
                    y_min = col ;
                }
            }
        }
        if (left == 2)
        {
            for (int ver = k; ver > col; ver--)
            {
                printf("%i[%i][%i] ", array[ver][col], ver, col);
                if (array[ver][col] < min)
                {
                    min = array[ver][col];
                    x_min = ver ;
                    y_min = col ;
                }
            }
            for (int hor = col; hor < (d - (col - 1)); hor++)
            {
                printf("%i[%i][%i] ", array[col][hor], col, hor);
                if (array[col][hor] < min)
                {
                    min = array[col][hor];
                    x_min = col ;
                    y_min = hor ;
                }
            }
        }
        printf("\nMinimum is %i[%i][%i]\n", min, x_min, y_min);
        int max = array[n - 1][n - 1];
        int x_max = n - 1 ;
        int y_max = n - 1 ;
        left = n % 3;
        num = n / 3;
        k = 0;
        d = n - 1;
        int col1 = n - 1;
        printf("Lower the diagonal:\n");
        for (int col = n - 1; col >= n - num; col--)
        {
            for (int dia1 = col; dia1 >= k; dia1--)
            {
                for (int dia2 = k; dia2 < col; dia2++)
                {
                    if (dia1 + dia2 == d)
                    {
                        printf("%i[%i][%i] ", array[dia1][dia2], dia1, dia2);
                        if (array[dia1][dia2] > max)
                        {
                            max = array[dia1][dia2];
                            x_max = dia1 ;
                            y_max = dia2 ;
                        }
                    }
                }
            }
            for (int ver = k; ver < col; ver++)
            {
                printf("%i[%i][%i] ", array[ver][col], ver, col);
                if (array[ver][col] > max)
                {
                    max = array[ver][col];
                    x_max = ver ;
                    y_max = col ;
                }
            }
            for (int hor = col; hor > k; hor--)
            {
                printf("%i[%i][%i] ", array[col][hor], col, hor);
                if (array[col][hor] > max)
                {
                    max = array[col][hor];
                    x_max = col ;
                    y_max = hor ;
                }
            }
            d++;
            k = k + 2;
            col1 = col;
        }
        if (left == 1)
        {
            for (int dia1 = col1; dia1 >= k; dia1--)
            {
                for (int dia2 = k; dia2 < (col1 + 1); dia2++)
                {
                    if (dia1 + dia2 == d)
                    {
                        printf("%i[%i][%i] ", array[dia1][dia2], dia1, dia2);
                        if (array[dia1][dia2] > max)
                        {
                            max = array[dia1][dia2];
                            x_max = dia1 ;
                            y_max = dia2 ;
                        }
                    }
                }
            }
        }
        else if ((left == 2) && (num != 0))
        {
            for (int dia1 = col1; dia1 >= k; dia1--)
            {
                for (int dia2 = k; dia2 < col1; dia2++)
                {
                    printf("%i[%i][%i] ", array[dia1][dia2], dia1, dia2);
                    if (dia1 + dia2 == d)
                    {
                        if (array[dia1][dia2] > max)
                        {
                            max = array[dia1][dia2];
                            x_max = dia1 ;
                            y_max = dia2 ;
                        }
                    }
                }
            }
            for (int ver = k + 1; ver < col1; ver++)
            {
                printf("%i[%i][%i] ", array[ver][col1 - 1], ver, (col1 - 1));
                if (array[ver][col] > max)
                {
                    max = array[ver][col];
                    x_max = ver ;
                    y_max = col ;
                }
            }
        }
        else if ((left == 2) && (num == 0))
        {
            printf("%i[%i][%i] ", array[n - 1][0], n - 1, 0);
            if (array[n - 1][0] > max)
            {
                max = array[n - 1][0];
                x_max = n - 1 ;
                y_max = 0 ;
            }
            printf("%i[%i][%i] ", array[0][n - 1], 0, n - 1);
            if (array[0][n - 1] > max)
            {
                max = array[0][n - 1];
                x_max = 0 ;
                y_max = n - 1 ;
            }
            printf("%i[%i][%i] ", array[n - 1][n - 1], n - 1, n - 1);
            if (array[n - 1][n - 1] > max)
            {
                max = array[n - 1][n - 1];
                x_max = n - 1 ;
                y_max = n - 1 ;
            }
        }
        printf("\nMaximum %i[%i][%i]\n", max, x_max, y_max);
    }
}