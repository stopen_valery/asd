#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"
int main()
{
    srand(time(0));
    int n = 0;
    int m = 0;
    printf("Input the size of matrix:\n");
    scanf("%i %i", &n, &m);
    int array[n][m];
    int num = 0;
    if (n < m || n == m)
    {
        num = (n * n - n) / 2;
    }
    else
    {
        num = (n * m) - (m * m + m) / 2;
    }
    int checkarr[num];
    bool same;
    printf("The matrix %ix%i:\n", n, m);
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m;)
        {
            same = false;
            int random = rand() % (n * m * 2 - 10 + 1) + 10;
            for (int s1 = 0; s1 < n; s1++)
            {
                for (int s2 = 0; s2 < m; s2++)
                {
                    if (random == array[s2][s1])
                    {
                        same = true;
                    }
                }
            }
            if (same == false)
            {
                array[i][j] = random;
                printf("%3i ", array[i][j]);
                j++;
            }
        }
        putchar('\n');
    }
    int k = 1;
    int check = 0;
    printf("The elements which will be sorted:\n");
    for (int ver = 0; ver < m; ver++)
    {
        for (int hor = k; hor < n; hor++)
        {
            if (hor == ver)
            {
                break;
            }
            else
            {
                checkarr[check] = array[hor][ver];
                printf("%i[%i] ", checkarr[check], check);
                check++;
            }
        }
        k++;
    }
    putchar('\n');
    int buf = 0;
    for (int i = 1; i <= num; i++)
    {
        bool flag = false;
        for (int j = 1; j <= num - i; j++)
        {
            if (checkarr[j] > checkarr[j - 1])
            {
                flag = true;
                buf = checkarr[j];
                checkarr[j] = checkarr[j - 1];
                checkarr[j - 1] = buf;
            }
        }
        if (flag == false)
        {
            break;
        }
    }
    printf("Sorted array is now:\n");
    for (int i = 0; i < num; i++)
    {

        printf("%i[%i] ", checkarr[i], i);
    }
    putchar('\n');
     check = 0;
     k = 1;
     for (int ver = 0; ver < m; ver++)
     {
         for (int hor = k; hor < n; hor++)
         {
             if (hor == ver)
             {
                 break;
             }
             else
             {
                 array[hor][ver] = checkarr[check];
                 check++;
             }
         }
         k++;
     }
    printf("The new matrix is:\n");
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (i > j)
            {
                printf(KRED "%3i " RESET, array[i][j]);
            }
            else
            {
                printf("%3i ", array[i][j]);
            }
        }
        putchar('\n');
    }
}