#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"
struct DLNode
{
    int data;
    struct DLNode *next;
    struct DLNode *prev;
};
struct SLNode
{
    int data;
    struct SLNode *next;
};
struct DLNode *create_DLNode(int data);
struct SLNode *create_SLNode(int data);
//
void print_DLNode(struct DLNode *print_node);
int num_SLNode (struct SLNode *num_node) ;
void print_SLNode(struct SLNode *print_node);
//
struct DLNode *max(struct DLNode *search_max);
struct DLNode *add_DLNode(struct DLNode *add_node, int data);
struct SLNode *add_SLNode(struct SLNode *add_node, int data) ;
struct SLNode *create_second_SLNode (struct SLNode *new_node, struct DLNode *take_from) ;
//
void free_SLNode(struct SLNode *free_node) ;
struct DLNode *remove_DLNode(struct DLNode *del_node);
void free_DLNode(struct DLNode *free_node);
int main()
{
    srand(time(0));
    struct DLNode *head = {0, NULL, NULL};
    struct SLNode *node = {0, NULL} ;
    printf("Choose one of the options\n"
         "1. Create DLNode\n"
         "2. Add node before maximum\n"
         "3. Remove nodes with negative non doubles\n"
         "4. Exit\n");
    while (1)
    {
        int n = 0;
        scanf("%i", &n);
        if (n == 1)
        {
            printf(KGRN "1. Creating node\n" RESET);
            if (head != NULL)
            {
                free_DLNode(head);
            }
            head = create_DLNode(rand() % (50 + 50 + 1) - 50);
            print_DLNode(head);
        }
        else if (n == 2)
        {
            int new_data = rand() % (50 + 50 + 1) - 50;
            printf(KGRN "2. Added element %i before maximum:\n" RESET , new_data);
            head = add_DLNode(head, new_data);
            print_DLNode(head);
        }
        else if (n == 3)
        {
            printf(KGRN "3. Removing nodes with negative doubles\n" RESET);
            node = create_second_SLNode(node, head);
            print_SLNode(node) ;
            head = remove_DLNode(head);
            print_DLNode(head);
        }
        else if (n == 4)
        {
            break;
        }
        else
        {
            printf(KRED "ERROR. An unknown task\n" RESET);
        }
    }
    free_DLNode(head);
    free_SLNode(node) ;
}
struct DLNode *create_DLNode(int data)
{
    struct DLNode *init_node = malloc(sizeof(struct DLNode));
    if (init_node == NULL)
    {
        printf (KRED "ERROR. Memory was not allocated\n" RESET);
        exit(1);
    }
    init_node->data = data;
    init_node->next = NULL;
    init_node->prev = NULL;
    return init_node;
}
struct SLNode *create_SLNode(int data)
{
    struct SLNode *init_node = malloc(sizeof(struct SLNode));
    if (init_node == NULL)
    {
        printf (KRED "ERROR. Memory was not allocated\n" RESET);
        exit(1);
    }
    init_node->data = data;
    init_node->next = NULL;
}
struct SLNode *create_second_SLNode (struct SLNode *new_node, struct DLNode *take_from)
{
    if(take_from == NULL)
    {
        return NULL ;
    }
    else
    {
        while(take_from != NULL)
        {
            if(take_from->data %2 != 0 && take_from->data <0)
            {
                new_node = add_SLNode(new_node, take_from->data) ; 
            }
            take_from = take_from->next ;
        }
    }
    return new_node ;
}
void print_DLNode(struct DLNode *print_node)
{
    int number = 0;
    if (print_node == NULL)
    {
        printf(KRED "ERROR. The DLNode is empty\n" RESET);
    }
    else
    {
        printf(KMAG"DLNode: " RESET) ;
        while ((print_node != NULL))
        {
            printf("%i ", print_node->data);
            number++;
            print_node = print_node->next;
        }
        putchar('\n');
        printf(KMAG"Number of DLnodes: " RESET);
        printf("%i\n", number) ;
    }
}
int num_SLNode (struct SLNode *num_node)
{
    int num = 0 ;
    while(num_node != NULL)
    {
        num_node = num_node->next ;
        num ++ ;
    }
    return num ;
}
void print_SLNode(struct SLNode *print_node)
{
    int number = num_SLNode(print_node);
    if (print_node == NULL)
    {
        printf(KRED "ERROR. The SLNode is empty\n" RESET);
    }
    else
    {
        printf(KCYN"SLNode: " RESET) ;
        while (print_node != NULL)
        {
            printf("%i ", print_node->data);
            print_node = print_node->next;
        }
        putchar('\n') ;
        printf(KCYN "Number of SLnodes: " RESET) ;
        printf("%i\n", number) ;
    }
}
struct DLNode *max(struct DLNode *search_max)
{
    struct DLNode *max = search_max;
    while (search_max != NULL)
    {
        if (search_max->data > max->data)
        {
            max = search_max;
        }
        search_max = search_max->next;
    }
    return max;
}
struct DLNode *add_DLNode(struct DLNode *add_node, int data)
{
    struct DLNode *max_node = max(add_node);
    struct DLNode *add = create_DLNode(data);
    if (add_node == NULL)
    {
        printf(KRED "Previous mode is empty\n" RESET);
        return add;
    }
    else if (max_node == add_node)
    {
        add->next = add_node;
        add_node->prev = add;
        return add;
    }
    else
    {
        struct DLNode *add = create_DLNode(data);
        struct DLNode *p_max = max_node->prev;
        struct DLNode *buffer = p_max->next;
        p_max->next = add;
        add->next = buffer;
        add->prev = p_max;
        if (buffer != NULL)
        {
            buffer->prev = add;
        }
        while (add->prev != NULL)
        {
            add = add->prev;
        }
        return add;
    }
}
struct SLNode *add_SLNode(struct SLNode *add_node, int data)
{
    if (add_node == NULL)
    {
        add_node = create_SLNode(data);
        return add_node;
    }
    struct SLNode *head = add_node;
    int num = num_SLNode(add_node);
    num = num / 2 - 1;
    for (int i = 0; i <= num; i++)
    {
        add_node = add_node->next;
    }
    struct SLNode *next = add_node->next;
    add_node->next = create_SLNode(data);
    add_node->next->next = next;
    return head;
}
struct DLNode *remove_DLNode(struct DLNode *del_node)
{
    if(del_node == NULL)
    {
        printf(KRED "ERROR.The node is empty\n" RESET) ;
        return NULL ;
    }
    int nul = 0 ;
    struct DLNode *next = {0, NULL, NULL};
    struct DLNode *prev = {0, NULL, NULL};
    struct DLNode *head = del_node;
    if (del_node->data % 2 != 0 && del_node->data < 0)
    {
        int count = 0 ;
        del_node = del_node->next ;
        while (del_node != NULL)
        {
            if (del_node->data % 2 != 0 && del_node->data < 0)
            {
                count ++ ;
            }
            del_node = del_node->next ;
        }
        if(count == 0)
        {
            head = head->next ;
            if(head == NULL)
            {
                return NULL ;
            }
            return head ;
        }
    }
    del_node = head ;
    while (del_node != NULL)
    {
        if (del_node->data % 2 != 0 && del_node->data < 0)
        {
            nul++ ;
            next = del_node->next;
            prev = del_node->prev;
            if (prev != NULL)
            {
                prev->next = del_node->next;
            }
            if (next != NULL)
            {
                next->prev = del_node->prev;
            }
            free(del_node);
        }
        del_node = del_node->next;
    }
    if(nul == 0)
    {
       printf(KRED "ERROR. No such elements\n" RESET) ;
        return head ;
    }
    while (prev->prev != NULL)
    {
        prev = prev->prev;
    }
    return prev;
}
void free_DLNode(struct DLNode *free_node)
{
    while (free_node != NULL)
    {
        struct DLNode *next = free_node->next;
        free(free_node);
        free_node = next;
    }
}
void free_SLNode(struct SLNode *free_node)
{
    while(free_node !=NULL)
    {
        struct SLNode *next = free_node->next ;
        free(free_node) ;
        free_node = next ;
    }
}
