#include <stdlib.h>
#include <stdio.h>
void print_arr(int size, int *array);
int main()
{
    int array[14] = {20, 441, 588, 8, 909, 8, 54, 15, 345, 85, 11, 1, 45, 744};
    int size = 14;
    int max = array[0];
    for (int i = 0; i < size; i++)
    {
        if (array[i] > max)
        {
            max = array[i];
        }
    }
    print_arr(size, array);
    int n = 0;
    printf("%i\n", max) ;
    while (1)
    {
        if (max == 0)
        {
            break;
        }
        max = max/10;
        n++;
    }
    printf("%i\n", n);
}
void print_arr(int size, int *array)
{
    for (int i = 0; i < size; i++)
    {
        printf("%i ", array[i]);
    }
    putchar('\n');
}